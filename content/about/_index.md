+++
title = "About Me"
date = 2024-01-31
+++

![Faraz Jawed](pic.jpeg)

Hello! I'm Faraz Jawed, a Master of Science in Data Science student at Duke University. My academic journey has been a blend of rigorous training in data science and computer science, with a focus on fields like Natural Language Processing, Statistical Modeling, and Machine Learning.

Before joining Duke, I completed my BSc (Honors) in Finance with a minor in Computer Science from Lahore University of Management Sciences. My passion for data science has led me to explore various domains, including finance, product development, and research.

Professionally, I have honed my skills as a Product Data Scientist at Baraka Financial Limited in Dubai, where I played a pivotal role in driving growth through data-driven strategies. 

I am deeply involved in research and projects that push the boundaries of data science. From developing a multimodal tool for AI-generated live commentary in sports to analyzing sentiment on social media platforms, my work reflects a commitment to innovative solutions.

I'm enthusiastic about sports, chess, travel, and investing in various markets. My journey is a blend of diverse experiences, reflecting a blend of analytical prowess and creative problem-solving.

[View my resume](https://drive.google.com/file/d/1JdJTiLUx2mkwqrr4efjenWJtzKEadK2M/view?usp=sharing)
